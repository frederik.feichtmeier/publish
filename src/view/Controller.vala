/* Publish.vala *
 * Copyright (C) 2018 Frederik Feichtmeier *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gtk;
using Cairo;

public class Controller : Gtk.Application {

	private Window mainWindow;
	private Builder builder;
	private Button addPageBtn;
	private Button zoomInBtn;
	private Button zoomOutBtn;
	private Button printBtn;
	private PrintOperation printOperation;
	private ToggleButton fullscreenBtn;
	private ToggleButton fullscreenFullscreenBtn;
	private ToggleButton rightPanelBtn;
	private ToggleButton leftPanelBtn;
	private ToggleButton selectBtn;
	private Revealer fullscreenRevealer;
	private Revealer rightPanelRevealer;
	private Revealer leftPanelRevealer;
	private Revealer editRevealer;
	private Gtk.ListStore listStore;
	private Gtk.TreeIter iter;
	private IconView pagesIconView;
	private Gtk.IconTheme icon_theme;
	private ListBox pagesListBox;
	private int pagesIndex = 0;

	protected override void activate() {

		builder = new Builder();

		try {
			builder.add_from_file("/home/yaru/publish/src/view/View.ui");
			mainWindow = builder.get_object ("mainWindow") as Window;
			this.mainWindow.application = this;
			builder.connect_signals(null);

			pagesListBox = builder.get_object("pagesListBox") as ListBox;

			addPageBtn = builder.get_object("addPageBtn") as Button;
			addPageBtn.clicked.connect(() => {
				try {
					listStore.append(out iter);
					Gdk.Pixbuf pixbuf = icon_theme.load_icon ("x-office-document", 20, 0);
					pagesIndex++;
					string pagesIndexString =  "%d".printf(pagesIndex);
					listStore.set(iter, 0, pixbuf, 1, pagesIndexString);
				} catch (Error e) {
					assert_not_reached ();
				}
			});

			zoomInBtn = builder.get_object("zoomInBtn") as Button;
			//zoomInBtn.button_press_event().connect (() => {	});

			zoomOutBtn = builder.get_object("zoomOutBtn") as Button;

			printBtn = builder.get_object("printBtn") as Button;
			printBtn.clicked.connect (() => {	
				printOperation = new PrintOperation();				
			});

			rightPanelBtn = builder.get_object("rightPanelBtn") as ToggleButton;
			rightPanelBtn.toggled.connect (() => {	
				if (rightPanelBtn.active) {
					rightPanelRevealer.show();
				} else {
					rightPanelRevealer.hide();
				}
			});
			leftPanelBtn = builder.get_object("leftPanelBtn") as ToggleButton;
			leftPanelBtn.toggled.connect (() => {	
				if (leftPanelBtn.active) {
					leftPanelRevealer.show();
				} else {
					leftPanelRevealer.hide();
				}
			});

			selectBtn = builder.get_object("selectBtn") as ToggleButton;
			selectBtn.toggled.connect (() => {	
				if (selectBtn.active) {
					editRevealer.show();
				} else {
					editRevealer.hide();
				}
			});

			fullscreenRevealer = builder.get_object("fullscreenRevealer") as Revealer;
			rightPanelRevealer = builder.get_object("rightPanelRevealer") as Revealer;
			leftPanelRevealer = builder.get_object("leftPanelRevealer") as Revealer;
			editRevealer = builder.get_object("editRevealer") as Revealer;

			fullscreenBtn = builder.get_object("fullscreenBtn") as ToggleButton;
			fullscreenBtn.toggled.connect (() => {	
				mainWindow.fullscreen();
				fullscreenFullscreenBtn.set_active(true);
				fullscreenRevealer.show();
			});

			fullscreenFullscreenBtn = builder.get_object("fullscreenFullscreenBtn") as ToggleButton;
			fullscreenFullscreenBtn.toggled.connect (() => {
				mainWindow.unfullscreen();
				fullscreenBtn.set_active(false);
				fullscreenRevealer.hide();
			});

			listStore = new Gtk.ListStore(2, typeof (Gdk.Pixbuf), typeof (string));

			pagesIconView = builder.get_object("pagesIconView") as IconView;
			pagesIconView.set_model(listStore);
			pagesIconView.set_pixbuf_column (0);
			pagesIconView.set_text_column (1);
			icon_theme = Gtk.IconTheme.get_default ();
			try {
				listStore.append(out iter);
				Gdk.Pixbuf pixbuf = icon_theme.load_icon ("x-office-document", 20, 0);
				listStore.set(iter, 0, pixbuf, 1, "1");
				pagesIndex++;
			} catch (Error e) {
				assert_not_reached ();
			}

			pagesIconView.selection_changed.connect (() => {
				List<Gtk.TreePath> paths = pagesIconView.get_selected_items ();
				Value title;
				Value icon;

				foreach (Gtk.TreePath path in paths) {
					bool tmp = listStore.get_iter (out iter, path);
					assert (tmp == true);

					listStore.get_value (iter, 0, out icon);
					listStore.get_value (iter, 1, out title);
					stdout.printf ("%s: %p\n", (string) title, ((Gdk.Pixbuf) icon));
				}
			});


			this.mainWindow.show_all();
			this.fullscreenRevealer.hide();
			this.editRevealer.hide();

		} catch (Error e) {
			error("Unable to load file: %s", e.message);
		}
	}

	public Controller() {
		Object(application_id: "org.gnome.Publish", flags: ApplicationFlags.FLAGS_NONE);
	}
}
