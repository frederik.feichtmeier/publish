### Publish

Publish is/should become a desktop publishing application similiar to Scribus for the GNOME desktop. It is in a very early state and not usable at all :)

Future feature wishlist:

- create prints and PDFs in either a pt-, cm- or pixel-based grid
- insert pictures or text frames with basic customisation option
- use the GNOME Hig for the whole UI

![firstScreenshot](https://i.imgur.com/xMvFKW7.png)